CREATE VIEW ManageEmployee AS
 SELECT Employee.EmployeeID, Employee.EmployeeName, Employee.EmployeeSurname,
	Department.DeptName,
	Manager.ManagerName, Manager.ManagerSurname
 FROM Manager JOIN Department ON Manager.DeptID = Department.DeptID JOIN Employee ON Manager.ManagerID = Employee.ManagerID;
